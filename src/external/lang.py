#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-12-18 13:57:20
# @Last Modified by:   chandan
# @Last Modified time: 2015-12-20 12:26:59

import nltk

NOUN_TAGS = ("NP", "NNP")

# find the proper nounds in text
def get_proper_nouns(text):
	nouns = []

	tokens = nltk.word_tokenize(text)
	tagged = nltk.pos_tag(tokens)

	# proper nouns have the NP tag
	nouns = [word for (word, pos) in tagged if pos in NOUN_TAGS]

	return nouns

def main():
	pass

if __name__ == '__main__':
	main()

