#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-12-17 19:07:27
# @Last Modified by:   chandan
# @Last Modified time: 2015-12-21 11:50:18

import db_handler as db
import misc
import learn

import MySQLdb
import datetime as dt

from sklearn.cross_validation import train_test_split
from sklearn import metrics

# db connection
DBCONN = MySQLdb.connect("localhost",
						"root",
						"diablo",
						"extracted_data",
						 charset='utf8', use_unicode=True
						)

def main():
	# get data 
	start_date = dt.date(2009, 1, 1)
	end_date = dt.date(2009, 1, 3)

	articles = db.get_articles(DBCONN, start_date, end_date)
	
	# split into train and test
	train_set, test_set = train_test_split(articles, test_size=0.2)
	
	# make training data - get prices for companies mentioned in articles
	train_set = misc.add_price_change_to_articles(train_set)

	# convert articles to count vectors
	train_points, count_model, tfidf_model = misc.article_to_vector(train_set, None, None)
	targets = train_set['pricediff'].values

	# train
	model = learn.train(train_points, targets)

	# test
	test_set = misc.add_price_change_to_articles(test_set)
	test_target = test_set['pricediff']
	test_points, _, _ = misc.article_to_vector(test_set,  count_model, tfidf_model)
	predictions = learn.test(model, test_points)

	# evaluate results
	print predictions
	print(metrics.classification_report(test_target, predictions))

if __name__ == '__main__':
	main()