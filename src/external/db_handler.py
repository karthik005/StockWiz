#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-11-23 19:21:19

import MySQLdb
import datetime as dt

from pprint import pprint

import pandas as pd

# get articles from database in the given date range
def get_articles(dbconn, start_date, end_date):
	articles = []

	cursor = dbconn.cursor()

	query = "select * from news_articles where pub_date between '{}' and '{}'"
	query = query.format(str(start_date), str(end_date))

	print "Executing: ", query

	try:
		cursor.execute(query)
	except:
		print "Could not read articles"

	articles = list(cursor.fetchall())

	# get column names
	num_fields = len(cursor.description)
	columns = [col[0] for col in cursor.description]

	cursor.close()

	return pd.DataFrame(articles, columns=columns)

if __name__ == '__main__':
	dbconn = MySQLdb.connect("localhost",
						"root",
						"password123",
						"extracted_data",
						 charset='utf8', use_unicode=True
						)

	start_date = dt.date(2009, 1, 1)
	end_date = dt.date(2009, 1, 1)

	articles = get_articles(dbconn, start_date, end_date)

	print "Got %d articles" % len(articles)
	pprint(articles[0])
