#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-12-11 11:48:07
# @Last Modified by:   chandan
# @Last Modified time: 2015-12-21 11:40:40

from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

def train(points, targets):
	model = None
	# train 

	# model = MultinomialNB().fit(points, targets)
	model = SGDClassifier(loss='hinge', penalty='l2', class_weight = "auto",
			alpha=1e-3, n_iter=5, random_state=42)

	model.fit(points, targets)

	return model

def test(model, points):
	predictions = []

	predictions = model.predict(points)

	return predictions

def main():
	pass

if __name__ == '__main__':
	main()