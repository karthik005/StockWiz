#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-12-18 18:25:04
# @Last Modified by:   chandan
# @Last Modified time: 2015-12-21 11:46:22

import pandas as pd
from pandas.io.data import DataReader
import numpy as np

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer

import Levenshtein

import datetime as dt

import lang

# min Levenshtein ratio for a match
MATCH_THRESHOLD = 0.7

# get symbol, company name and industry
def get_cnx_symbols(filename):
	csv_content =  pd.read_csv(filename)
	# Using the CNX List - get only symbol and company name
	cnx = csv_content.loc[:, ("Symbol", "Company Name", "Industry")]

	cnx.rename(columns={'Company Name':'Name'}, inplace=True)

	return cnx

COMPANIES = get_cnx_symbols('../../resources/ind_cnx500list.csv')

# find the best matching company with word
def get_company_match(word):
	symbol = None
	ratio = None

	func = lambda company: Levenshtein.ratio(unicode(company.lower()), word.lower())

	matches = COMPANIES['Name'].map(func)
	idx = matches.idxmax()
	row = COMPANIES.iloc[[idx]]

	symbol, ratio = row['Symbol'], matches.iloc[idx]

	return (symbol, ratio)


MISLEADS = ['bank', 'india', 'indian']

# find the ticker symbol with the
# highest match with text
def get_companies_from_article(text):
	best_match_symbol = None

	nouns = lang.get_proper_nouns(text)

	nouns = filter(lambda x: x.lower() not in MISLEADS, nouns)
	# find the best match for each noun
	matches = map(get_company_match, nouns)

	# pick the best of these
	best_matches = filter(lambda match: match[1] > MATCH_THRESHOLD, matches)
	# best_matches = map(lambda match: match[0], best_matches)
	try:
		best_match = max(best_matches, key=lambda x: x[1])
		best_match_symbol = best_match[0]
	except ValueError:
		# no matches above threshold
		best_match_symbol = np.NaN

	return best_match_symbol

# find the price difference for symbol -
# price[date+1] - price[date]
def get_price_diff(stock_symbol, date):
	# 0 = monday
	# 6 = sunday

	# go backward if start date is on a weekend
	start_date = date

	if start_date.weekday() == 5:
		start_date = start_date - dt.timedelta(days=1)
	elif start_date.weekday() == 6:
		start_date = start_date - dt.timedelta(days=2)

	# go forward if end date is on a weekend
	end_date = start_date + dt.timedelta(days=1)

	if end_date.weekday() == 5:
		end_date = end_date + dt.timedelta(days=2)
	elif start_date.weekday() == 6:
		end_date = end_date + dt.timedelta(days=1)

	try:
		history = DataReader(stock_symbol+'.NS', 'yahoo', start_date, end_date)
	except IOError:
		try:
			history = DataReader(stock_symbol[:-1]+'.NS', 'yahoo', start_date, end_date)	
		except IOError:
			try:
				history = DataReader(stock_symbol, 'yahoo', start_date, end_date)		
			except IOError:
				try:
					history = DataReader(stock_symbol[:-1], 'yahoo', start_date, end_date)
				except IOError:
					history = []

 
	if len(history) == 0:
		return np.NaN

	values = history['Adj Close'].values
	# print values
	# difference of last and first values
	diff = values[-1] - values[0]

	return diff

# add a new column to the df 
# where new col = diff in prices of stock on the day after the article
# and the day of the article
def add_price_change_to_articles(articles):
	articles_with_prices = articles.copy()
	
	# for each article find the best matching company

	articles_with_prices['companymatch'] = articles_with_prices['content'].map(
															get_companies_from_article
														)

	# remove articles with no matches
	# articles_with_prices.dropna(subset=['companymatch'], how='any', inplace=True)
	articles_with_prices.dropna(inplace = True)

	# get price changes for these companies
	articles_with_prices['pricediff'] = articles_with_prices.apply(lambda row: 
								1 if get_price_diff(row['companymatch'],
													 row['pub_date']) > 0
								else -1,
								axis=1
							)
	
	articles_with_prices.dropna(inplace = True)

	return articles_with_prices

# form count vectors from df containing articles
def article_to_vector(articles, count_model = None, tfidf_model = None):
	if count_model is None and tfidf_model is None:
		count_model = CountVectorizer()
		counts = count_model.fit_transform(articles['content'])

		tfidf_model = TfidfTransformer()
		train_tfidf = tfidf_model.fit_transform(counts)

	else:
		counts = count_model.transform(articles['content'])
		train_tfidf = tfidf_model.transform(counts)
		
	return train_tfidf, count_model, tfidf_model


def main():
	pass

if __name__ == '__main__':
	main()