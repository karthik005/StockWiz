#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-11-20 13:44:04

from gensim.models import Word2Vec
import nltk as nlp
import string

 
test_sentences = [u"Bank NPAs continue to remain unacceptable: Jaitley", 
 u"Pfizer to Buy Allergan in Record $160 Billion Deal",
 u"Nifty ends tad below 7850, Sensex flat; ITC, Hindalco fall",
 u"Bupa to acquire addtnl 23% stake in Max Bupa for Rs 191 cr",
 u"Sequent Scientific to Acquire Turkey's Topkim",
]

def remove_punctuation(input_str):
	not_letters_or_digits = u''.join(string.punctuation)
	translate_table = dict((ord(char), None) for char in not_letters_or_digits)

	translate_table[u'?'] = u' '
	translate_table[u'!'] = u' '
	translate_table[u'.'] = u' '

	return input_str.translate(translate_table)

def main():
	sentences = test_sentences
	sentences = map(remove_punctuation, sentences)
	tokenized_sentences = map(lambda x:x.lower().split(), sentences)

	model = Word2Vec(sentences)

	print model.similarity('Pfizer', 'Bank')
if __name__ == '__main__':
	main()


