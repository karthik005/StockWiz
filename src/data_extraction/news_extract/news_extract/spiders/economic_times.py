#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-10-25 16:09:43
import scrapy
from news_extract.items import *
import MySQLdb as msq
import datetime as dt

import json
import helper_funcs as hf
import MySQLdb


start_date = dt.date(2009, 1, 8)
end_date = dt.date(2009, 1, 31)

# set according to start_date (look up on http://economictimes.indiatimes.com/archive.cms)
start_time = 39821
token_list = []

base_url = "http://economictimes.indiatimes.com"
date_iter = start_date

db_conn = MySQLdb.connect("localhost","root","diablo","extracted_data", charset='utf8', use_unicode=True)

# populate token list
while date_iter <= end_date:
	token_list.append((date_iter, start_time))
	start_time += 1
	date_iter = date_iter + dt.timedelta(1)

class EconomictimesSpider(scrapy.Spider):
    '''
    scrapy spider for economic times
    '''
    name = "economic_times"
    allowed_domains = ["indiatimes.com"]
    start_urls = ["http://economictimes.indiatimes.com/archivelist/year-"+str(dt.year)+",month-"+str(dt.month)+",starttime="+str(start_time)+".cms" for (dt, start_time) in token_list]


    def parse(self, response):
        links = response.xpath('/html/body/div[3]/div/div[6]/div[1]/span/table[2]/tr/td/ul/li/a/@href')


        for link in links:
        	url = base_url + link.extract()
        	(date, start_time) = token_list[self.start_urls.index(response.url)] if response.url in self.start_urls else (None,None)
        	if date is not None:
	        	yield scrapy.Request(url, meta={'date':date}, callback=self.process_article)

    def process_article(self, response):
		'''
		parse article, process and insert into database
		'''
		item = NewsExtractItem()

		item['title'] = "".join(response.xpath('//*[@id="pageContent"]/article/h1/text()').extract())
		item['date'] = response.meta['date']

		content = hf.remove_tags("".join(response.xpath('//*[@id="pageContent"]/article/div[@class="artText"]').extract()))

		item['content'] = " ".join(content.split())
		item['url'] = response.url
		item['urlh'] = hf.generate_urlh(response.url)

		if item['title'] and item['content']:
			hf.write_to_dbase(db_conn, item)
			db_conn.commit()
			yield item