#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Karthik
# @Date:   2015-10-26 19:26:32

# import xml

# def remove_tags(text):
#     return ''.join(xml.etree.ElementTree.fromstring(text).itertext())

import re
import MySQLdb
import hashlib


def remove_tags(text):
	'''
	remove html tags
	'''
	TAG_RE = re.compile(r'<[^>]+>')
	return TAG_RE.sub('', text)

def write_to_dbase(db_conn, article_item):
	''' 
	insert article_item into database 
	'''
	
	cursor = db_conn.cursor()
	try:
		cursor.execute("INSERT INTO news_articles VALUES(%s,%s,%s,%s,%s)",(article_item['url'],str(article_item['date']), article_item['title'], article_item['content'], article_item['urlh']))
	except:
		print "Duplicate Entry"
	cursor.close()
	return

def generate_urlh(article_url):
	'''
	generate hash of the url for use as primary key
	'''
	return hashlib.md5(article_url).hexdigest()