#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-11-04 12:33:36

import misc
import stock_data as sd
from datetime import date, timedelta
import trade_test as tt
import generate_stats as gs

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import finance
from matplotlib.collections import LineCollection
from sklearn import cluster, covariance, linear_model
from math import log

from sklearn.cross_validation import train_test_split
# from helper_functions import *
import johansen as jo

def cluster_stocks(feature_matrix):
	'''
	cluster stocks based on covariance of features
	'''
	edge_model = covariance.GraphLassoCV()
	X = feature_matrix.copy().T
	X /= X.std(axis=0)
	edge_model.fit(X)
	return cluster.affinity_propagation(edge_model.covariance_)


def johansen_test(close_prices):
	'''
	generate johansen test statistics for prices series
	'''
	p = 0
	k = 3
	x = np.array(close_prices)
	result = jo.coint_johansen(x,p,k)
	return result

def check_cointegration(jo_result):
	'''
	check a johansen result object for cointegration
	'''
	tr_stats = jo_result.lr1
	eig_stats = jo_result.lr2
	tr_crit_vals = jo_result.cvt
	eig_crit_vals = jo_result.cvm
	tr_crit = tr_crit_vals[-1, 0] # trace critical value at 90%
	eig_crit = tr_crit_vals[-1, 0] # eigen critical value at 90%
	# print "tr"
	# print tr_stats
	# print tr_crit_vals
	# print "eig"
	# print eig_stats
	# print eig_crit_vals
	if tr_stats[-1] > (tr_crit) and eig_stats[-1] > (eig_crit):
		return True

	return False



def find_cointegrating_stocks(group_close_prices, lookback):
	'''
	find the cointegrating stocks among a group (industry) of stocks
	'''
	
	window_close_prices = group_close_prices.iloc[:lookback,:]
	feature_matrix = abs(np.array(group_close_prices.diff().dropna())).T
	_, labels = cluster_stocks(feature_matrix)
	n_labels = labels.max()
	stock_names = group_close_prices.columns
	cointegrated_stocks = {}

	for i in range(n_labels + 1):
		symbol_cluster = stock_names[labels == i]
		print('Cluster %i: %s' % ((i + 1), ', '.join(symbol_cluster)))
		if len(symbol_cluster) > 1:
			clustered_prices = np.array(group_close_prices.loc[:, [col for col in symbol_cluster]].iloc[:lookback,:])
			result = johansen_test(clustered_prices)
			print "Cointegrating: ", check_cointegration(result)
			if check_cointegration(result):
				hedge_vector =  result.evec[:,0]
				cointegrated_stocks[tuple(symbol_cluster)] = (hedge_vector,result)
		else:
			print "Cointegrating: False"

	return cointegrated_stocks


def generate_coint_weights(trade_weights, close_prices, date_range, cointegrated_stocks, lookback):
	'''
	generate trade weights for cointegrated stocks
	'''
	# trade_weights = pd.DataFrame(index = date_range, np.zeros((date_range, close_prices.shape[1])))
	start_date, end_date = date_range
	num_days = end_date-start_date+1
	for coint_stock_grp in cointegrated_stocks:
		print "Generating trade weights for group: ", coint_stock_grp
		hedge_vector, _ = cointegrated_stocks[coint_stock_grp]
		stock_names = coint_stock_grp
		sp_ranged = np.array(close_prices.loc[start_date-lookback:end_date+1, stock_names])
		weighted_prices = np.sum(np.tile(hedge_vector, [sp_ranged.shape[0],1])*sp_ranged, axis = 1)
		weighted_prices = pd.DataFrame(weighted_prices, index = range(start_date-lookback, end_date+1))


		for date in xrange(start_date, end_date+1):
			coint_price = np.array(weighted_prices.loc[date-1, :])
			coint_mean = np.array(weighted_prices.loc[date-30:date,:].mean())
			coint_std = np.array(weighted_prices.loc[date-30:date,:].std())
			base = -(coint_price - coint_mean)/coint_std
			# print base

			day_trade_weights = np.tile(base, [1,len(stock_names)])*(hedge_vector*coint_price)
			trade_weights.loc[date, stock_names] = day_trade_weights

	# mr_stock_names = list(mean_reverting_stocks.columns)
	return trade_weights





def main_coint():
	'''
	main function for testing cointegration 
	'''
	start_date = date(2014, 01, 01)
	train_upto = date(2014, 05, 01)
	end_date = date(2014, 07, 01)
	excluded_industries = [0] # industry indices to exclude 
	run_upto_industry = 3 #run upto this industry

	'''
	group companies by Industry
	'''
	all_companies = misc.get_cnx_symbols('../../resources/ind_cnx500list.csv')
	industries = all_companies.groupby("Industry")
	count = 0
	pnls = []
	rets = []
	for industry_name, companies in industries:
		if count in excluded_industries:
			count+=1
			continue
		if count == run_upto_industry:
			break
		count+=1
		print "Industry: ", industry_name
		symbols = np.array(companies["Symbol"].values)
		# names = np.array(companies["Name"].values)

		# get test and train data
		train_frames = sd.get_history(symbols, start_date, train_upto)
		close_train_prices = train_frames['Close']		
		test_frames = sd.get_history(symbols, train_upto, end_date)
		close_test_prices = test_frames['Close']
		# find lookback(train) period and number of test days
		LOOKBACK = close_train_prices.shape[0]
		test_days = close_test_prices.shape[0]
		
		# calculate the number of days
		num_days = close_train_prices.shape[0]+close_test_prices.shape[0]

		# generate concatenated price series (train+test)
		close_prices = pd.concat([close_train_prices, close_test_prices])
		close_prices = close_prices.reset_index()
		close_prices.drop(['index'], axis = 1, inplace = True)

		# initialize trade weights to 0 for duration
		trade_weights = pd.DataFrame(np.zeros((test_days, close_prices.shape[1])), index = range(LOOKBACK,num_days), columns = list(close_prices.columns))

		# find the cointegrated stocks in the industry
		cointegrated_stocks = find_cointegrating_stocks(close_prices, LOOKBACK)

		# generate trade weights for the above stocks
		trade_weights = generate_coint_weights(trade_weights, close_prices, (LOOKBACK, num_days-1), cointegrated_stocks, LOOKBACK)
		
		# generate returns and pnl
		pnls.append(tt.trade_test(trade_weights, (LOOKBACK, num_days-1), close_prices))
		rets.append(gs.find_daily_returns(pnls[-1], trade_weights))

	# plot cumulative stats
	pnl = reduce(lambda x,y: x+y, pnls)
	ret = reduce(lambda x,y: x+y, rets)
	gs.plot_cumulative_pnl(pnl)
	gs.plot_cumulative_returns(ret)
		
	return
	

if __name__ == '__main__':
	main_coint()

