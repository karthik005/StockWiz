#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Karthik
# @Date:   2015-12-21 23:19:50


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def find_daily_returns(daily_pnl, trade_weights):
	rets = np.array(daily_pnl)/np.sum(abs(np.array(trade_weights)), axis = 1)
	rets = pd.DataFrame(rets, index = trade_weights.index)
	rets = rets.fillna(0)
	return rets


def plot_cumulative_pnl(daily_pnl):
	cum_pnl = daily_pnl.cumsum()
	pnl_plot = cum_pnl.plot()
	pnl_plot.set_xlabel("time (days)")
	pnl_plot.set_ylabel("cumulative pnl")
	plt.show()
	return

def plot_cumulative_returns(daily_returns):
	cum_returns = daily_returns.cumsum()
	ret_plot = cum_returns.plot()
	ret_plot.set_xlabel("time (days)")
	ret_plot.set_ylabel("cumulative returns")
	plt.show()
	return