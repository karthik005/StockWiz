#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-12-14 18:55:56
import misc
import stock_data as sd
from datetime import date


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import finance
from matplotlib.collections import LineCollection
from sklearn import cluster, covariance, manifold, linear_model
from statsmodels.tsa.stattools import adfuller
from math import log
from pprint import pprint
from sklearn.cross_validation import train_test_split
from stock_selection import filter_by_param
import generate_stats as gs

# find the mean reverting stocks
def find_mean_reverting(close_prices, num_days = 100):
	mean_reverting_stocks = pd.DataFrame(index = ['adf_stat', 'p_val', 'lag', 'reg_vals', 'half_life'])
	for stock in close_prices.columns:
		adf_stat,p_val,_,reg_results = adfuller(np.array(close_prices[stock][-num_days-1:]), maxlag = 3, regresults = True)
		# pprint(vars(reg_results))
		crit_vals = reg_results.critvalues
		if adf_stat <= crit_vals['5%']:
			# print reg_results.resols.params
			half_life = int(-log(2)/reg_results.resols.params[0])
			mean_reverting_stocks[stock] = [adf_stat, p_val, reg_results.usedlag, reg_results.resols.params, half_life]

	return mean_reverting_stocks

def train_generate_weights(close_prices, date_range, lookback = 100):
	'''
	generates weights for single mean reverting stocks
	'''
	consistent = check_consistency(close_prices, date_range, lookback)
	if not consistent:
		raise ValueError("close_prices not of required length")

	start_date, end_date = date_range
	num_days = end_date-start_date+1
	trade_weights = pd.DataFrame(np.zeros((num_days, close_prices.shape[1])), index = range(start_date, end_date+1), columns = list(close_prices.columns))
	mean_reverting_stocks = mr.find_mean_reverting(close_prices[start_date-lookback:start_date], lookback)
	# print mean_reverting_stocks
	trade_weights = mr.generate_mr_boll_weights(trade_weights, close_prices, date_range, mean_reverting_stocks)
	return trade_weights

def generate_mr_weights(trade_weights, close_prices, date_range, mean_reverting_stocks):
	'''
	generate mean reverting stocks' trade weights
	'''
	# trade_weights = pd.DataFrame(index = date_range, np.zeros((date_range, close_prices.shape[1])))
	start_date, end_date = date_range
	num_days = end_date-start_date+1
	mr_stock_names = list(mean_reverting_stocks.columns)
	windows = pd.DataFrame(index = mr_stock_names, columns = ['window'])
	windows['window'] = [max(2*mean_reverting_stocks.loc['half_life', stock_name], 20) for stock_name in mr_stock_names]
	for date in xrange(start_date, end_date+1):
		stock_prices = np.array(close_prices.loc[date-1,mr_stock_names])
		window_dfs = [close_prices.loc[date-windows.loc[stock_name,'window']:date, stock_name] for stock_name in mr_stock_names]

		stock_means = np.array([window_df.mean() for window_df in window_dfs])
		stock_stdev = np.array([window_df.std() for stock_name in mr_stock_names])
		trade_weights.loc[date, mr_stock_names] = -(stock_prices-stock_means)/stock_stdev
	return trade_weights




def generate_mr_boll_weights(trade_weights, close_prices, date_range, mean_reverting_stocks):
	'''
	generate mean reverting stocks' trade weights. use bollinger bands to decide whether to trade
	'''
	# trade_weights = pd.DataFrame(index = date_range, np.zeros((date_range, close_prices.shape[1])))
	start_date, end_date = date_range
	num_days = end_date-start_date+1
	mr_stock_names = list(mean_reverting_stocks.columns)
	windows = pd.DataFrame(index = mr_stock_names, columns = ['window'])
	windows['window'] = [max(2*mean_reverting_stocks.loc['half_life', stock_name], 20) for stock_name in mr_stock_names]

	for date in xrange(start_date, end_date+1):
		stock_prices = np.array(close_prices.loc[date-1,mr_stock_names])
		window_dfs = [close_prices.loc[date-windows.loc[stock_name,'window']:date, stock_name] for stock_name in mr_stock_names]

		stock_means = np.array([window_df.mean() for window_df in window_dfs])
		stock_stdev = np.array([window_df.std() for stock_name in mr_stock_names])
		# print stock_prices
		# print stock_means
		bollinger_bool = stock_prices > stock_means+1.5*stock_stdev
		bollinger_bool += stock_prices < stock_means-1.5*stock_stdev
		# print bollinger_bool
		trade_weights.loc[date, mr_stock_names] = -((stock_prices-stock_means)/stock_stdev)*bollinger_bool
		
	return trade_weights



def main():
	start_date = date(2014, 10, 01)
	end_date = date(2015, 07, 01)
	excluded_industries = [0,1]
	run_upto_industry = 3

	'''
	group companies by Industry
	'''
	all_companies = misc.get_cnx_symbols('../../resources/ind_cnx500list.csv')
	industries = all_companies.groupby("Industry")
	count = 0
	pnls = []

	for industry_name, companies in industries:
		if count in excluded_industries:
			count+=1
			continue
		elif count == run_upto_industry:
			break
		count+=1
		print "Industry: ", industry_name
		symbols = np.array(companies["Symbol"].values)
		names = np.array(companies["Name"].values)

		all_frames = sd.get_history(symbols, start_date, end_date)
		filtered = filter_by_param(all_frames, 'Volume')
		# print filtered

		close_prices = filtered['Adj Close']
		date_range = ((close_prices.index)[0]+100, close_prices.index[-1])
		trade_weights = pred.train_generate_weights_ss(close_prices, date_range, 100)
		# print trade_weights

		pnls.append(tt.trade_test(trade_weights, date_range, close_prices))


	pnl = reduce(lambda x,y: x+y, pnls)
	print "pnls"
	print pnl
	# gs.plot_cumulative_pnl(pnl)
	rets = gs.find_daily_returns(pnl, trade_weights)
	gs.plot_cumulative_returns(rets)

	return

if __name__ == '__main__':
	main()