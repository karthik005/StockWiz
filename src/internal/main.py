#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Karthik
# @Date:   2015-12-21 21:38:49
'''
main module to be executed. combines both single stock and multistock strategies
'''

from datetime import date
import numpy as np
import pandas as pd
from sklearn import cluster, covariance, manifold, linear_model
from statsmodels.tsa.stattools import adfuller
from math import log
from pprint import pprint
from sklearn.cross_validation import train_test_split

import misc
import stock_data as sd
import prediction as pred
import trade_test as tt
from stock_selection import filter_by_param
import generate_stats as gs

def main():
	start_date = date(2014, 01, 01)
	train_upto = date(2014, 05, 01)
	end_date = date(2014, 07, 01)
	excluded_industries = []
	run_upto_industry = 2

	'''
	group companies by Industry
	'''
	all_companies = misc.get_cnx_symbols('../../resources/ind_cnx500list.csv')
	industries = all_companies.groupby("Industry")
	count = 0
	pnls = []
	rets = []
	for industry_name, companies in industries:
		if count in excluded_industries:
			count+=1
			continue
		elif count == run_upto_industry:
			break
		count+=1
		print "Industry: ", industry_name
		symbols = np.array(companies["Symbol"].values)

		# get test and train data
		print "\nFetching train data"
		train_frames = sd.get_history(symbols, start_date, train_upto)
		close_train_prices = train_frames['Close']	

		print "\nFetching test data"
		test_frames = sd.get_history(symbols, train_upto, end_date)
		close_test_prices = test_frames['Close']

		# find lookback(train) period and number of test days
		lookback = close_train_prices.shape[0]
		test_days = close_test_prices.shape[0]
		
		# calculate the totalnumber of days
		num_days = close_train_prices.shape[0]+close_test_prices.shape[0]
		
		# generate concatenated price series (train+test)
		close_prices = pd.concat([close_train_prices, close_test_prices])
		close_prices = close_prices.reset_index()
		close_prices.drop(['index'], axis = 1, inplace = True)
		
		date_range = ((close_prices.index)[0]+lookback, close_prices.index[-1])
		trade_weights_mr = pred.train_generate_weights_ss(close_prices, date_range, lookback)
		trade_weights_coint = pred.train_generate_weights_ms(close_prices, lookback, test_days)
		
		trade_weights = trade_weights_mr + trade_weights_coint
		
		# generate returns and pnl
		pnls.append(tt.trade_test(trade_weights, date_range, close_prices))
		rets.append(gs.find_daily_returns(pnls[-1], trade_weights))

	# print and plot cumulative stats
	pnl = reduce(lambda x,y: x+y, pnls)
	ret = reduce(lambda x,y: x+y, rets)
	print "pnls"
	print pnl	
	print "returns"
	print ret
	gs.plot_cumulative_pnl(pnl)
	gs.plot_cumulative_returns(ret)

	return

if __name__ == '__main__':
	main()