#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: karthik
# @Date:   2015-12-21 11:31:32

def filter_by_param(stock_data, param, num_days = 50, threshold_param_val = 0):
	param_data = stock_data[param]
	filtered_stock_data = stock_data.copy()
	
	if threshold_param_val != 0:
		filtered_param_cols = param_data[:num_days].mean(axis=0) > threshold_vol
	
	else:
		mean_vols = param_data[:num_days].mean()
		avg_means = mean_vols.mean()
		std_dev = mean_vols.std()
		filtered_param_cols = mean_vols > avg_means-(std_dev/2)

	for stock_param in stock_data:
		current_frame = stock_data[stock_param]
		filtered_stock_data[stock_param]  = current_frame.loc[:,filtered_param_cols]

	return filtered_stock_data

