import pandas as pd
import numpy as np
from pandas.io.data import DataReader
from datetime import datetime

# symbols: list of ticker symbols
# start_date: date object
# end_date: date object
# 
# return: data frame with columns = symbols, rows = date range
def get_history(symbols,  start_date, end_date):
	interval_length = (end_date - start_date).days
	num_days = 0
	params = ['Open','High','Low','Close','Volume','Adj Close']
	result_dict = {i:None for i in params}

	for symbol in symbols:
		history = get_history_yahoo(symbol, start_date, end_date)
		if len(history) == 0:
			continue

		# keep history only if there are prices for more
		# than half the interval 
		# then create the result data frame
		for param in params:
			history_param = history[param]
			# print history_param
			if len(history) > interval_length/2 and result_dict[param] is None:
				num_days = len(history_param)
				result_dict[param] = pd.DataFrame(columns=symbols, index=range(num_days))

			# discard if data missing
			if result_dict[param] is not None and len(history_param) == num_days:
				result_dict[param][symbol] = history_param.values

	for param in result_dict:
		# remove columns with missing values
		if result_dict[param] is not None:
			result_dict[param] = result_dict[param].dropna(axis=1, how='any')

	return result_dict

# start_date: date object
# end_date: date object
# stock_symbol: ticker symbol
# 
# return: adjusted closing prices in given date range
def get_history_yahoo(stock_symbol, start_date, end_date):

	print "Searching for: ", stock_symbol, " ... ", 
	history = []
	# try NSE symbol
	try:
		history = DataReader(stock_symbol+'.NS', 'yahoo', start_date, end_date)
	except IOError:
		try:
			history = DataReader(stock_symbol[:-1]+'.NS', 'yahoo', start_date, end_date)	
		except IOError:
			try:
				history = DataReader(stock_symbol, 'yahoo', start_date, end_date)		
			except IOError:
				try:
					history = DataReader(stock_symbol[:-1], 'yahoo', start_date, end_date)
				except IOError:
					history = []

	print len(history), "days"

	return history

# history: data frame with columns = ticker symbols
# train_days: number of days of history in each data point
# predict_company: company for which predictions are made (Y)

# return: data frame (X, Y)
def get_points_from_history(history, predict_company, train_days):
	num_days = len(history)

	# not enough history to have 'train_days' features in each point
	if num_days < train_days:
		return None

	companies = list(history.columns)
	days = range(1, train_days + 1)

	result_columns = [company + day for company in companies 
									for day in map(str, days)] 
	result_columns.append("NextDay")

	result = pd.DataFrame(columns=result_columns)

	# starting day for a data point
	for start in xrange(num_days - train_days):
		new_point = np.empty(0)
		end = start + train_days - 1
		
		for company in companies:
			company_history = history.loc[start:end, company].values
			new_point = np.append(new_point, company_history)

		next_day_val = history.loc[end+1, predict_company]
		new_point = np.append(new_point, next_day_val)

		result.loc[len(result) + 1] = new_point

	return result

if __name__ == '__main__':
	main()