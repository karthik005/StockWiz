#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Karthik
# @Date:   2015-12-21 21:10:13

from datetime import date
import numpy as np
import pandas as pd
from sklearn import cluster, covariance, manifold, linear_model
from statsmodels.tsa.stattools import adfuller
from math import log
from pprint import pprint
from sklearn.cross_validation import train_test_split
import mean_reversion as mr
import cointegration as coint
# import cointegration as co

def check_consistency(close_prices, date_range, lookback):
	start_date, end_date = date_range
	indices = close_prices.index
	return set(range(start_date-lookback, end_date+1)).issubset(set(indices))

def train_generate_weights_ss(close_prices, date_range, lookback = 100):
	'''
	finds and generates weights for single mean reverting stocks
	'''
	consistent = check_consistency(close_prices, date_range, lookback)
	if not consistent:
		raise ValueError("close_prices not of required length")

	start_date, end_date = date_range
	num_days = end_date-start_date+1
	trade_weights = pd.DataFrame(np.zeros((num_days, close_prices.shape[1])), index = range(start_date, end_date+1), columns = list(close_prices.columns))
	mean_reverting_stocks = mr.find_mean_reverting(close_prices[start_date-lookback:start_date], lookback)
	# print mean_reverting_stocks
	trade_weights = mr.generate_mr_boll_weights(trade_weights, close_prices, date_range, mean_reverting_stocks)
	return trade_weights

def train_generate_weights_ms(close_prices, lookback, test_days):
	'''
	finds and generates weights for multiple cointegrating stocks
	'''
	if lookback+test_days < close_prices.shape[0]:
		 raise ValueError("close_prices not of required length")

	num_days = lookback+test_days

	# initialize trade weights to 0 for duration
	trade_weights = pd.DataFrame(np.zeros((test_days, close_prices.shape[1])), index = range(lookback,num_days), columns = list(close_prices.columns))

	# find the cointegrated stocks in the industry
	cointegrated_stocks = coint.find_cointegrating_stocks(close_prices, lookback)

	# generate trade weights for the above stocks
	trade_weights = coint.generate_coint_weights(trade_weights, close_prices, (lookback, num_days-1), cointegrated_stocks, lookback)

	return trade_weights
