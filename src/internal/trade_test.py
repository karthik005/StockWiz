#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: Karthik
# @Date:   2015-12-21 20:53:06


import pandas as pd

def trade_test(trade_weights, date_range, close_prices):
	start_date, end_date = date_range
	stock_names = close_prices.columns
	pnl_matrix = pd.DataFrame(index = range(start_date, end_date+1), columns = stock_names)
	for date in xrange(start_date, end_date+1):
		price_deltas = close_prices.loc[date,:] - close_prices.loc[date-1,:]
		pnl_matrix.loc[date,:] = (trade_weights.loc[date,:]*price_deltas)/close_prices.loc[date-1, :]

	pnl = pnl_matrix.sum(axis = 1)
	return pnl