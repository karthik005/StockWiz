#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: chandan
# @Date:   2015-10-04 10:28:41
# @Last Modified by:   chandan
# @Last Modified time: 2015-10-26 12:18:12

import pandas as pd

# get symbol, company name and industry
def get_nasdaq_symbols(filename):
	ndq =  pd.read_csv(filename)
	# concat sector and industry
	ndq["Industry"] = ndq["Sector"] + "|" + ndq["industry"]
	# get only symbol, company name and industry
	ndq = ndq.loc[:, ("Symbol", "Name", "Industry")]

	return ndq

# get symbol, company name and industry
def get_cnx_symbols(filename):
	csv_content =  pd.read_csv(filename)
	# Using the CNX List - get only symbol and company name
	cnx = csv_content.loc[:, ("Symbol", "Company Name", "Industry")]

	cnx.rename(columns={'Company Name':'Name'}, inplace=True)

	return cnx
